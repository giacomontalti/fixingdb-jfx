package app;

import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private Stage primaryStage;

    @Override
    public void start(final Stage primaryStage) throws IllegalArgumentException, IOException {
        this.primaryStage = primaryStage;
        ApplicationManager.setRefToPrimaryStage(this);
        ApplicationManager.changeScene("LoginView");
        ApplicationManager.setWindowTitle("Login");
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
    //Lista dei metodi accessibili dall'application controller:

    public void setScene(Scene scene) {
        this.primaryStage.setScene(scene);
        this.primaryStage.show();
    }

    public void setTitle(String newTitle) {
        this.primaryStage.setTitle(newTitle);
    }

    public void centerOnScreen() {
        this.primaryStage.centerOnScreen();
    }
}
