-- Database Section
-- ________________ 

create database FIXING_DB;
     use FIXING_DB;


-- Tables Section
-- _____________ 

create table CITTA (
     ID int UNSIGNED AUTO_INCREMENT,
     Nome varchar(255) not null,
     constraint IDCITTA primary key (ID));

create table CLIENTE (
     ID int UNSIGNED AUTO_INCREMENT,
     Nome varchar(255) not null,
     Cognome varchar(255) not null,
     Indirizzo varchar(255),
     Recapito varchar(255) not null,
     Email varchar(255),
     IDCitta int(11) UNSIGNED not null,
     constraint IDCLIENTE primary key (ID));

create table DISPOSITIVO (
     Modello varchar(255),
     Seriale char(10) not null,
     IDTipologia int(11) UNSIGNED not null,
     IDMarca int(11) UNSIGNED not null,
     constraint IDDISPOSITIVO primary key (Seriale));

create table INTERVENTO (
     IDRiparazione bigint(20) UNSIGNED,
     Data_Inizio datetime,
     Data_Fine datetime not null,
     Descrizione varchar(2000) not null,
     Utente varchar(20) not null,
     constraint IDINTERVENTO primary key (IDRiparazione, Data_Inizio));


create table MARCA (
     ID int UNSIGNED AUTO_INCREMENT,
     Nome varchar(255) not null,
     constraint IDMARCA primary key (ID));

create table RIPARAZIONE (
     ID SERIAL,
     SerialeDispositivo char(10) not null,
     IDCliente int(11) UNSIGNED not null,
     Problema_Dichiarato varchar(2000) not null,
     Visto_Acceso boolean not null,
     Ha_Alimentatore boolean not null,
     Seriale_Alimentatore varchar(255),
     Ha_Batteria boolean not null,
     Seriale_Batteria varchar(255),
     Accessori_Note varchar(2000),
     Data_Riconsegna_Richiesta date,
     Data_Entrata timestamp DEFAULT CURRENT_TIMESTAMP,
     è_Emergenza boolean not null,
     IDStato int(11) UNSIGNED not null DEFAULT 1,
     constraint ID primary key (ID));

create table STATO (
     ID int UNSIGNED AUTO_INCREMENT,
     Descrizione varchar(255) not null,
     constraint IDSTATO primary key (ID));

create table TIPOLOGIA (
     ID int UNSIGNED AUTO_INCREMENT,
     Testo varchar(255) not null,
     constraint IDTIPOLOGIA primary key (ID));

-- Constraints Section
-- ___________________ 

alter table CLIENTE add constraint FKAbita
foreign key (IDCitta)
references CITTA (ID);

alter table DISPOSITIVO add constraint FKAppartiene
foreign key (IDTipologia)
references TIPOLOGIA (ID);

alter table DISPOSITIVO add constraint FKProdotto
foreign key (IDMarca)
references MARCA (ID);

alter table INTERVENTO add constraint FKSubisce
foreign key (IDRiparazione)
references RIPARAZIONE (ID);

alter table RIPARAZIONE add constraint FKSi_trova_in
foreign key (IDStato)
references STATO (ID);

alter table RIPARAZIONE add constraint FKPorta
foreign key (IDCliente)
references CLIENTE (ID);

alter table RIPARAZIONE add constraint FKRiceve
foreign key (SerialeDispositivo)
references DISPOSITIVO (Seriale);

-- Populate Stato Database
-- _______________________
INSERT INTO `stato`(`Descrizione`) VALUES ("Da iniziare");
INSERT INTO `stato`(`Descrizione`) VALUES ("In lavorazione");
INSERT INTO `stato`(`Descrizione`) VALUES ("Completato, da chiamare");
INSERT INTO `stato`(`Descrizione`) VALUES ("Cliente in arrivo");
INSERT INTO `stato`(`Descrizione`) VALUES ("Ritirato");
INSERT INTO `stato`(`Descrizione`) VALUES ("Chiamare per preventivo");
INSERT INTO `stato`(`Descrizione`) VALUES ("Preventivo Accettato");
INSERT INTO `stato`(`Descrizione`) VALUES ("Preventivo Rifiutato");