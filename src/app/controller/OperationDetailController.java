package app.controller;

import app.model.Operation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class OperationDetailController {

    @FXML
    private Label dataInizio;

    @FXML
    private Label oraInizio;

    @FXML
    private Label dataFine;

    @FXML
    private Label oraFine;

    @FXML
    private Label username;

    @FXML
    private TextArea descrizione;

    @FXML
    void close(ActionEvent event) {
        Stage stage = (Stage) this.descrizione.getScene().getWindow();
        stage.close();
    }
    
    public void setData(Operation operation){
        this.dataInizio.setText(operation.getInizio().toLocalDate().toString());
        this.oraInizio.setText(operation.getInizio().toLocalTime().toString());
        this.dataFine.setText(operation.getFine().toLocalDate().toString());
        this.oraFine.setText(operation.getFine().toLocalTime().toString());
        this.descrizione.setText(operation.getDescrizione());
        this.username.setText(operation.getUtente());
    }

}
