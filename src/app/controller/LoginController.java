package app.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import app.ApplicationManager;
import app.model.login.LoginModel;
import app.model.login.LoginModelImpl;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;

public class LoginController implements Initializable {

    final static private String DEFAULT_SERVER_ADDRESS = "localhost";

    @FXML
    private JFXTextField user;

    @FXML
    private JFXPasswordField password;

    @FXML
    private JFXButton login_btn;

    @FXML
    private JFXButton reset_btn;

    @FXML
    private JFXTextField serverAddress;

    private LoginModel model;

    @FXML
    void login(ActionEvent event) {

        try {
            // Provo a collegarmi al database con le credenziali inserite
            this.model.tryConnection(this.serverAddress.getText(), this.user.getText(), this.password.getText());

            try {
                // Provo a cambiare scena
                ApplicationManager.changeScene("HomeView");
                ApplicationManager.setWindowTitle("Home");

            } catch (FileNotFoundException e) {
                ApplicationManager.viewSimpleAlert(null, "Non riesco a trovare il file HomeView.fxml", AlertType.ERROR);

            } catch (IllegalArgumentException e) {
                ApplicationManager.viewSimpleAlert(null, "Path al file fxml vuoto o uguale a null", AlertType.ERROR);

            } catch (IOException e) {
                ApplicationManager.viewSimpleAlert(null, "Incongruenze tra fxml controller", AlertType.ERROR);
            }

        } catch (ClassNotFoundException e1) {
            ApplicationManager.viewSimpleAlert(null, "Non posso caricare il driver di jdbc", AlertType.ERROR);

        } catch (SQLException e1) {
            ApplicationManager.viewSimpleAlert(null, "Impossibile contattare il server o credenziali scorrette",
                    AlertType.WARNING);
        }

    }

    @FXML
    void reset(ActionEvent event) {
        this.user.setText("");
        this.password.setText("");
        this.serverAddress.setText("");
    }

    @FXML
    void resetServer(ActionEvent event) {
        this.serverAddress.setText(DEFAULT_SERVER_ADDRESS);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.model = new LoginModelImpl();
    }

}
