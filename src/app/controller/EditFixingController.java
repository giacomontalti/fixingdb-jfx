package app.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;

import app.ApplicationManager;
import app.database.FixingTable;
import app.database.FixingTable.FixingExtended;
import app.model.Fixing;
import app.model.StateFixing;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

public class EditFixingController implements Initializable {

    @FXML
    private JFXTextArea problema;

    @FXML
    private JFXCheckBox check_alimentatore;

    @FXML
    private JFXTextField text_alimentatore;

    @FXML
    private JFXCheckBox check_batteria;

    @FXML
    private JFXTextField text_batteria;

    @FXML
    private JFXTextArea note;

    @FXML
    private JFXDatePicker riconsegna;

    @FXML
    private JFXToggleButton bool_emergenza;

    @FXML
    private JFXToggleButton bool_vistoAcceso;

    @FXML
    private ComboBox<String> status;

    private FixingExtended fixing;

    @FXML
    void close(ActionEvent event) {
        this.closeWindow();
    }

    private void closeWindow() {
        Stage stage = (Stage) this.status.getParent().getScene().getWindow();
        stage.close();
    }

    @FXML
    void save(ActionEvent event) {
        Fixing f = new Fixing(problema.getText(), bool_vistoAcceso.isSelected(), check_alimentatore.isSelected(),
                text_alimentatore.getText(), check_batteria.isSelected(), text_batteria.getText(), note.getText(),
                riconsegna.getValue(), bool_emergenza.isSelected());
        try {
            FixingTable.updateFixing(this.fixing.getIdRiparazione(), this.status.getValue(), f);
            ApplicationManager.viewSimpleAlert("Modifica Effettuata.",
                    "Le modifiche alla riparazione sono state correttamente eseguite", AlertType.INFORMATION);
            ApplicationManager.updateView();

        } catch (ClassNotFoundException | SQLException e) {
            ApplicationManager.viewExceptionAlert(e, "Errore modifica riparazione!");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.status.setItems(StateFixing.getAsList());
    }

    public void setData(int idFixing) {
        try {
            this.fixing = FixingTable.getFixingById(idFixing);

            this.bool_emergenza.setSelected(this.fixing.isEmergenza());
            this.bool_vistoAcceso.setSelected(this.fixing.isVistoAcceso());
            this.check_alimentatore.setSelected(this.fixing.isHasAlimentatore());
            this.check_batteria.setSelected(this.fixing.isHasBatteria());
            this.note.setText(this.fixing.getNoteAccessori());
            this.problema.setText(this.fixing.getProblemi());
            this.riconsegna.setValue(this.fixing.getRiconsegna());
            this.text_alimentatore.setText(this.fixing.getAlimentatore());
            this.text_batteria.setText(this.fixing.getBatteria());
            this.status.getSelectionModel().select(this.fixing.getTextStato());
        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
