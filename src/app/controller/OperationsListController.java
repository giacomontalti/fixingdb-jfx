package app.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import app.ApplicationManager;
import app.database.OperationTable;
import app.model.Operation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class OperationsListController implements Initializable {

    @FXML
    private TableView<Operation> table;

    @FXML
    private TableColumn<Operation, LocalDateTime> dataInizioCol;

    @FXML
    private TableColumn<Operation, LocalDateTime> dataFineCol;

    @FXML
    private TableColumn<Operation, String> descrizioneCol;
    
    @FXML
    private TableColumn<Operation, String> utenteCol;

    @FXML
    private Label title;

    @FXML
    void viewOperation(ActionEvent event) {
        if (this.table.getSelectionModel().getSelectedItem() != null) {
            Stage stage = ApplicationManager.makeStage(Modality.WINDOW_MODAL, StageStyle.DECORATED,
                    "Dettaglio Intervento");
            FXMLLoader loader = ApplicationManager.createLoader("OperationDetailView");
            try {
                stage.setScene(new Scene(loader.load()));
                OperationDetailController controller = loader.getController();
                controller.setData(this.table.getSelectionModel().getSelectedItem());
                stage.show();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.dataInizioCol.setCellValueFactory(new PropertyValueFactory<>("inizio"));
        this.dataFineCol.setCellValueFactory(new PropertyValueFactory<>("fine"));
        this.descrizioneCol.setCellValueFactory(new PropertyValueFactory<>("descrizione"));
        this.utenteCol.setCellValueFactory(new PropertyValueFactory<>("Utente"));
    }

    public void setData(int idRiparazione) {
        try {
            this.table.setItems(OperationTable.getOperationsById(idRiparazione));
        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
