package app.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Optional;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;

import app.ApplicationManager;
import app.database.DispositivoTable;
import app.model.Device;
import app.model.Fixing;
import app.model.Person;
import app.model.StateFixing;
import app.model.home.HomeModel;
import app.model.home.HomeModelImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class HomeController implements Initializable {

    @FXML
    private JFXTextField txtfld_tipologia;

    @FXML
    private JFXTextField txtfld_marca;

    @FXML
    private JFXTextField txtfld_modello;

    @FXML
    private JFXTextField txtfld_seriale;

    @FXML
    private JFXTextArea txtarea_problemi;

    @FXML
    private JFXToggleButton bool_vistoAcceso;

    @FXML
    private JFXCheckBox bool_hasAlimentatore;

    @FXML
    private JFXTextField txtfld_alimentatore;

    @FXML
    private JFXCheckBox bool_hasBatteria;

    @FXML
    private JFXTextField txtfld_batteria;

    @FXML
    private JFXTextArea txtarea_note;

    @FXML
    private JFXDatePicker data_riconsegna;

    @FXML
    private JFXToggleButton check_isEmergenza;

    @FXML
    private JFXTextField txtfld_nome;

    @FXML
    private JFXTextField txtfld_cognome;

    @FXML
    private JFXTextField txtfld_recapito;

    @FXML
    private JFXTextField txtfld_città;

    @FXML
    private JFXTextField txtfld_indirizzo;

    @FXML
    private JFXTextField txtfld_email;

    @FXML
    private Button btn_daIniziare;

    @FXML
    private Button btn_daChiamare;

    @FXML
    private Button btn_emergenze;

    @FXML
    private Button btn_daRitirare;

    @FXML
    private Button btn_inLavorazione;

    @FXML
    private Button btn_riprendere;

    private HomeModel model;

    private AutoCompletionBinding<String> autoSuggestionTipologia;
    private AutoCompletionBinding<String> autoSuggestionMarca;
    private AutoCompletionBinding<String> autoSuggestionCittà;

    private final static String redString = "-fx-background-color: #EF3E36; -fx-text-fill: #FFFFFF";
    private final static String defaultString = "-fx-background-color: #F4F4F4; -fx-text-fill:#4C5B5C;";
    private final static String yellowString = "-fx-background-color: #FFE900; -fx-text-fill:#4C5B5C;";
    private final static String blueString = "-fx-background-color: #0C68D1; -fx-text-fill:#FFFFFF;";
    private final static String greenString = "-fx-background-color: #82D173; -fx-text-fill:#FFFFFF;";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.model = new HomeModelImpl();
        this.addAutoCompleteToTextFields();
        this.updateDashboard();
        ApplicationManager.setHomeController(this);
    }

    private void addAutoCompleteToTextFields() {
        try {
            this.autoSuggestionTipologia = TextFields.bindAutoCompletion(txtfld_tipologia, this.model.getTipologie());
            this.autoSuggestionMarca = TextFields.bindAutoCompletion(txtfld_marca, this.model.getMarche());
            this.autoSuggestionCittà = TextFields.bindAutoCompletion(txtfld_città, this.model.getCittà());
        } catch (ClassNotFoundException | SQLException e) {
            ApplicationManager.viewExceptionAlert(e, "Impossibile aggiungere i suggerimenti alle textbox!");
        }
    }

    public void updateDashboard() {
        this.updateLabel(this.btn_daIniziare, StateFixing.TO_START, yellowString);
        this.updateLabel(this.btn_emergenze, StateFixing.EMERGENCY, redString);
        this.updateLabel(this.btn_daRitirare, StateFixing.TO_DELIVER, greenString);
        this.updateLabel(this.btn_daChiamare, StateFixing.TO_CALL, blueString);
        this.updateLabel(this.btn_inLavorazione, StateFixing.WORKING_ON, blueString);
        this.updateLabel(this.btn_riprendere, StateFixing.TO_RESUMING, yellowString);
    }

    private void updateLabel(Button label, StateFixing state, String alertStyle) {
        int number = 0;

        try {
            number = this.model.getCountOf(state);
        } catch (ClassNotFoundException | SQLException e) {
            ApplicationManager.viewExceptionAlert(e, "Errore impostazione della dashboard:" + state.toString());
        }

        label.setText(Integer.toString(number));
        label.setStyle((number > 0 ? alertStyle : defaultString));
    }

    @FXML
    void doInserimento(ActionEvent event) {

        Optional<String> listEmptyFields = checkEmptyFields();
        // TODO Dovrei fare ogni try dentro l'altro per evitare che continui nel
        // caso in cui ci sia un errore?
        if (listEmptyFields.isPresent()) {
            ApplicationManager.viewSimpleAlert("Campi mancanti:", listEmptyFields.get(), AlertType.ERROR);
        } else {

            // Gestisco il dispositivo
            Device device = new Device(txtfld_seriale.getText(), txtfld_modello.getText(), txtfld_tipologia.getText(),
                    txtfld_marca.getText());

            try {
                this.model.correctDevice(device);
            } catch (ClassNotFoundException | SQLException e2) {
                ApplicationManager.viewExceptionAlert(e2, "Errore inserimento dispositivo!");
            }

            // Gestisco la persona
            Person person = new Person(txtfld_nome.getText(), txtfld_cognome.getText(), txtfld_recapito.getText(),
                    txtfld_città.getText(), txtfld_indirizzo.getText(), txtfld_email.getText());
            ObservableList<Person> listPossiblePerson = FXCollections.observableArrayList();

            try {
                listPossiblePerson = this.model.findPerson(person);
            } catch (ClassNotFoundException | SQLException e1) {
                ApplicationManager.viewExceptionAlert(e1, "Errore ricerca cliente!");
            }

            // Visualizzo o meno la seleziona della persona
            if (!listPossiblePerson.isEmpty()) {
                try {
                    this.makePersonTable(listPossiblePerson, person);
                } catch (IllegalArgumentException | IOException e) {
                    ApplicationManager.viewExceptionAlert(e, "Impossibile visualizzare tabella dei possibili clienti!");
                }
            } else {
                try {
                    this.model.addPerson(person);
                } catch (ClassNotFoundException | SQLException e) {
                    ApplicationManager.viewExceptionAlert(e, "Impossibile aggiungere il cliente!");
                }
            }

            Fixing fixing = new Fixing(this.model.getSelectedDevice().getSerial(),
                    this.model.getSelectedPerson().getId(), txtarea_problemi.getText(), bool_vistoAcceso.isSelected(),
                    bool_hasAlimentatore.isSelected(), txtfld_alimentatore.getText(), bool_hasBatteria.isSelected(),
                    txtfld_batteria.getText(), txtarea_note.getText(), data_riconsegna.getValue(),
                    check_isEmergenza.isSelected());
            try {
                this.model.addFixing(fixing);
            } catch (ClassNotFoundException | SQLException e) {
                ApplicationManager.viewExceptionAlert(e, "Impossibile aggiungere riparazione in ingresso!");
            }

            ApplicationManager.viewSimpleAlert("Riparazione Inserita!", "La riparazione è stata inserita!",
                    AlertType.INFORMATION);
            this.resetAll();
            this.updateDashboard();
        }
    }

    private Optional<String> checkEmptyFields() {
        String toReturn = new String();

        if (txtfld_seriale.getText().isEmpty()) {
            toReturn = toReturn + "Seriale Dispositivo\n";
        }

        if (txtfld_tipologia.getText().isEmpty()) {
            toReturn = toReturn + "Tipologia Dispositivo\n";
        }

        if (txtfld_marca.getText().isEmpty()) {
            toReturn = toReturn + "Marca Dispositivo\n";
        }

        if (txtarea_problemi.getText().isEmpty()) {
            toReturn = toReturn + "Problemi Dichiarati\n";
        }

        if (txtfld_nome.getText().isEmpty()) {
            toReturn = toReturn + "Nome Cliente\n";
        }

        if (txtfld_cognome.getText().isEmpty()) {
            toReturn = toReturn + "Cognome Cliente\n";
        }

        if (txtfld_recapito.getText().isEmpty()) {
            toReturn = toReturn + "Recapito Cliente\n";
        }

        if (txtfld_città.getText().isEmpty()) {
            toReturn = toReturn + "Città Cliente\n";
        }

        // Se tutti i campi necessari sono stati riempiti
        if (toReturn.isEmpty()) {
            return Optional.empty();
        }
        // Se manca qualcosa
        else {
            return Optional.of(toReturn);
        }
    }

    private void makePersonTable(ObservableList<Person> listPossiblePerson, Person person) throws IOException {
        // Creo la finestra
        do { // TODO Evitare il while, fare in modo che se clicco chiudi evita
             // di fare l'inserimento ( e magari visualizza un'alert)
            Stage stage = ApplicationManager.makeStage(Modality.APPLICATION_MODAL, StageStyle.DECORATED,
                    "Ricerca Persone");

            // Carico da file la view della tabella e il controller
            FXMLLoader fxmlLoader = ApplicationManager.createLoader("PersonTableView");
            stage.setScene(new Scene(fxmlLoader.load()));
            PersonTableController tableController = fxmlLoader.getController();

            // Aggiungo le persone trovate alla tabella e visualizzo
            tableController.setData(listPossiblePerson, person);
            // Passo il controllo del model alla view
            tableController.setHomeModel(this.model);

            stage.showAndWait();
        } while (this.model.getSelectedPerson() == null || this.model.getSelectedPerson().getId() == 0);
    }

    private void updateExistingAutoComplete() {
        this.autoSuggestionTipologia.dispose();
        this.autoSuggestionMarca.dispose();
        this.autoSuggestionCittà.dispose();
        this.addAutoCompleteToTextFields();
    }

    private void resetAll() {
        this.model.reset();
        this.bool_hasAlimentatore.setSelected(false);
        this.bool_hasBatteria.setSelected(false);
        this.bool_vistoAcceso.setSelected(false);
        this.check_isEmergenza.setSelected(false);
        this.data_riconsegna.setValue(null);
        this.txtarea_note.setText("");
        this.txtarea_problemi.setText("");
        this.txtfld_alimentatore.setText("");
        this.txtfld_batteria.setText("");
        this.txtfld_città.setText("");
        this.txtfld_cognome.setText("");
        this.txtfld_email.setText("");
        this.txtfld_indirizzo.setText("");
        this.txtfld_marca.setText("");
        this.txtfld_modello.setText("");
        this.txtfld_nome.setText("");
        this.txtfld_recapito.setText("");
        this.txtfld_seriale.setText("");
        this.txtfld_tipologia.setText("");
        this.updateExistingAutoComplete();
    }

    @FXML
    void updateView() {
        this.txtfld_alimentatore.setDisable((this.bool_hasAlimentatore.isSelected() ? false : true));
        this.txtfld_batteria.setDisable((this.bool_hasBatteria.isSelected() ? false : true));
    }

    @FXML
    void loadFromSerial(ActionEvent event) {
        Device device;
        try {
            device = DispositivoTable.loadFromDataBase(this.txtfld_seriale.getText());
            if (device != null) {
                this.txtfld_tipologia.setText(device.getTextTypology());
                this.txtfld_marca.setText(device.getTextBrand());
                this.txtfld_modello.setText(device.getModel());
            } else {
                ApplicationManager.viewSimpleAlert("Dispositivo non trovato!",
                        "Nessun dispositivo associato al seriale inserito!", AlertType.WARNING);
            }
        } catch (ClassNotFoundException | SQLException e) {
            ApplicationManager.viewExceptionAlert(e, "Impossibile caricare dal database il dispositivo!");
        }

    }

    @FXML
    void generaSeriale(ActionEvent event) {
        try {
            this.txtfld_seriale.setText(this.model.generateSeriale());
        } catch (ClassNotFoundException | SQLException e) {
            ApplicationManager.viewExceptionAlert(e, "Impossibile generare il seriale!");
        }
    }

    @FXML
    void view_daChiamare(ActionEvent event) throws IOException {
        this.viewFixingTable(StateFixing.TO_CALL);
    }

    @FXML
    void view_daIniziare(ActionEvent event) throws IOException {
        this.viewFixingTable(StateFixing.TO_START);
    }

    @FXML
    void view_daRitirare(ActionEvent event) throws IOException {
        this.viewFixingTable(StateFixing.TO_DELIVER);
    }

    @FXML
    void view_emergenze(ActionEvent event) throws IOException {
        this.viewFixingTable(StateFixing.EMERGENCY);
    }

    @FXML
    void view_archivio(ActionEvent event) throws IOException {
        this.viewFixingTable(StateFixing.PICKED_UP);
    }

    @FXML
    void view_inLavorazione(ActionEvent event) throws IOException {
        this.viewFixingTable(StateFixing.WORKING_ON);
    }

    @FXML
    void view_riprendere(ActionEvent event) throws IOException {
        this.viewFixingTable(StateFixing.TO_RESUMING);
    }

    private void viewFixingTable(StateFixing state) throws IOException {
        Stage stage = ApplicationManager.makeStage(Modality.WINDOW_MODAL, StageStyle.DECORATED,
                "Ricerca riparazioni:" + state.toString());

        // Carico da file la view della tabella e il controller
        FXMLLoader loader = ApplicationManager.createLoader("FixingTableView");
        stage.setScene(new Scene(loader.load()));
        FixingTableController controller = loader.getController();
        try {
            controller.setTableData(this.model.getListOfFixing(state));
            controller.setStateForUpdate(state);
        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        stage.showAndWait();
    }

    @FXML
    void updateDashboard(MouseEvent event) {
        this.updateDashboard();
    }

    @FXML
    void viewIpAddress(ActionEvent event) throws SocketException, UnknownHostException {
        String interfacesToShow = "Indirizzo Host: " + InetAddress.getLocalHost().getHostAddress() + "\n\n";
        Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
        while (n.hasMoreElements()) {
            NetworkInterface e = n.nextElement();
            Enumeration<InetAddress> a = e.getInetAddresses();
            if (a.hasMoreElements()) {
                interfacesToShow = interfacesToShow + e.getName() + "\n";
                while (a.hasMoreElements()) {
                    interfacesToShow = interfacesToShow + a.nextElement().getHostAddress() + "\n";
                }
                interfacesToShow = interfacesToShow + "\n";
            }
        }
        ApplicationManager.viewSimpleAlert("Interfacce disponibili:", interfacesToShow, AlertType.INFORMATION);
    }
}
