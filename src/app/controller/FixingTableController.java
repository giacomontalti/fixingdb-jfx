package app.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

import app.ApplicationManager;
import app.database.FixingTable;
import app.database.FixingTable.FixingExtended;
import app.model.StateFixing;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FixingTableController implements Initializable {

    @FXML
    private TableView<FixingExtended> table;

    @FXML
    private TableColumn<FixingExtended, String> descrizioneCol;

    @FXML
    private TableColumn<FixingExtended, String> modelloCol;

    @FXML
    private TableColumn<FixingExtended, String> tipologiaCol;

    @FXML
    private TableColumn<FixingExtended, String> marcaCol;

    @FXML
    private TableColumn<FixingExtended, String> problemaCol;

    @FXML
    private TableColumn<FixingExtended, LocalDate> dataEntrataCol;

    @FXML
    private TableColumn<FixingExtended, LocalDate> dataRichiestaCol; //TODO Local date può cambiare formato?

    @FXML
    private TableColumn<FixingExtended, Boolean> emergenzaCol;

    @FXML
    private TableColumn<FixingExtended, String> nomeCol;

    @FXML
    private TableColumn<FixingExtended, String> cognomeCol;

    @FXML
    private TableColumn<FixingExtended, String> recapitoCol;

    private StateFixing stateSelected;

    @FXML
    private Label title;

    @FXML
    void addNewOperation(ActionEvent event) {
        int idFixing = this.table.getSelectionModel().getSelectedItem().getIdRiparazione();
        Stage stage = ApplicationManager.makeStage(Modality.WINDOW_MODAL, StageStyle.DECORATED, "Aggiungi intervento");
        FXMLLoader loader = ApplicationManager.createLoader("NewOperationView");
        try {
            stage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        NewOperationController controller = loader.getController();
        controller.setItem(idFixing);
        stage.show();
        this.closeWindow();
    }

    @FXML
    void modifyFixing(ActionEvent event) {
        FixingExtended selectedFixing = this.table.selectionModelProperty().getValue().getSelectedItem();
        if (selectedFixing != null) {
            Stage stage = ApplicationManager.makeStage(Modality.WINDOW_MODAL, StageStyle.DECORATED,
                    "Modifica Riparazione");
            FXMLLoader loader = ApplicationManager.createLoader("EditFixingView");
            try {
                stage.setScene(new Scene(loader.load()));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            EditFixingController controller = loader.getController();

            controller.setData(selectedFixing.getIdRiparazione());
            stage.show();
        }
    }

    @FXML
    void viewOperations(ActionEvent event) {
        FixingExtended selectedFixing = this.table.selectionModelProperty().getValue().getSelectedItem();
        if (selectedFixing != null) {
            Stage stage = ApplicationManager.makeStage(Modality.WINDOW_MODAL, StageStyle.DECORATED,
                    "Lista Interventi Effettuati");
            FXMLLoader loader = ApplicationManager.createLoader("OperationsListView");
            try {
                stage.setScene(new Scene(loader.load()));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            OperationsListController controller = loader.getController();

            controller.setData(selectedFixing.getIdRiparazione());
            stage.show();
        }
    }

    public void setTableData(ObservableList<FixingExtended> list) {
        this.table.setItems(list);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.descrizioneCol.setCellValueFactory(new PropertyValueFactory<>("descrizioneStatus"));
        this.modelloCol.setCellValueFactory(new PropertyValueFactory<>("modello"));
        this.tipologiaCol.setCellValueFactory(new PropertyValueFactory<>("tipologia"));
        this.marcaCol.setCellValueFactory(new PropertyValueFactory<>("marca"));
        this.problemaCol.setCellValueFactory(new PropertyValueFactory<>("problemi"));
        this.dataEntrataCol.setCellValueFactory(new PropertyValueFactory<>("dataEntrata"));
        this.dataRichiestaCol.setCellValueFactory(new PropertyValueFactory<>("riconsegna"));
        this.emergenzaCol.setCellValueFactory(new PropertyValueFactory<>("textEmergenza"));
        this.nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
        this.cognomeCol.setCellValueFactory(new PropertyValueFactory<>("cognome"));
        this.recapitoCol.setCellValueFactory(new PropertyValueFactory<>("recapito"));
    }

    @FXML
    void update(MouseEvent event) {
        try {
            this.table.setItems(FixingTable.getFixingByState(this.stateSelected));
        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setStateForUpdate(StateFixing state) {
        this.stateSelected = state;
        this.title.setText(state.toString());
    }

    private void closeWindow() {
        Stage stage = (Stage) this.title.getScene().getWindow();
        stage.close();
    }
}
