package app.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.TextFields;

import com.jfoenix.controls.JFXTextField;

import app.ApplicationManager;
import app.database.CittàTable;
import app.database.PersonTable;
import app.model.Person;
import app.model.home.HomeModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class PersonTableController implements Initializable {

    @FXML
    private TableView<Person> table;

    @FXML
    private TableColumn<Person, String> nomeCol;

    @FXML
    private TableColumn<Person, String> cognomeCol;

    @FXML
    private TableColumn<Person, String> recapitoCol;

    @FXML
    private TableColumn<Person, String> cittàCol;

    @FXML
    private TableColumn<Person, String> indirizzoCol;

    @FXML
    private TableColumn<Person, String> emailCol;

    @FXML
    private JFXTextField cognomeText;

    @FXML
    private JFXTextField nomeText;

    @FXML
    private JFXTextField recapitoText;

    @FXML
    private JFXTextField indirizzoText;

    @FXML
    private JFXTextField cittàText;

    @FXML
    private JFXTextField emailText;

    private HomeModel homeModel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
        cognomeCol.setCellValueFactory(new PropertyValueFactory<>("cognome"));
        recapitoCol.setCellValueFactory(new PropertyValueFactory<>("recapito"));
        cittàCol.setCellValueFactory(new PropertyValueFactory<>("textCittà"));
        // TODO Fare in modo che questo campo sia preso da un oggetto di tipo
        // city?
        indirizzoCol.setCellValueFactory(new PropertyValueFactory<>("indirizzo"));
        emailCol.setCellValueFactory(new PropertyValueFactory<>("email"));
    }

    @FXML
    void createNewPerson(ActionEvent event) {
        if (this.isAllInserted()) {
            Person p = new Person(nomeText.getText(), cognomeText.getText(), recapitoText.getText(),
                    cittàText.getText(), indirizzoText.getText(), emailText.getText());
            try {
                PersonTable.addPerson(p);
            } catch (IllegalArgumentException | ClassNotFoundException | SQLException e) {
                ApplicationManager.viewExceptionAlert(e, "Errore insesrimento persona!");
            }
            this.homeModel.setPerson(p);
            this.closeWindow();
        } else {
            ApplicationManager.viewSimpleAlert("Cammpi Mancanti!", "Necessari:\nNome\nCognome\nRecapito\nCittà",
                    AlertType.WARNING);
        }
    }

    // TODO Cercare di unificare le cose anche con la HomeView
    private boolean isAllInserted() {
        if (!this.nomeText.getText().isEmpty() && !this.cognomeText.getText().isEmpty()
                && !this.recapitoText.getText().isEmpty() && !this.cittàText.getText().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @FXML
    void selectPerson(ActionEvent event) {
        Person p = this.table.getSelectionModel().getSelectedItem();
        if (p != null) {
            try {
                this.homeModel.setPerson(p);
            } catch (IllegalArgumentException e) {
                ApplicationManager.viewExceptionAlert(e, "La persona non ha un id valido!\nPersona: " + p.toString());
            }
            this.closeWindow();
        } else {
            ApplicationManager.viewSimpleAlert("Magari prima selezionane uno...", "Non hai selezionato nessun cliente!",
                    AlertType.ERROR);
        }
    }

    private void closeWindow() {
        Stage stage = (Stage) this.table.getScene().getWindow();
        stage.close();
    }

    void setHomeModel(HomeModel homemodel) {
        this.homeModel = homemodel;
    }

    public void setData(ObservableList<Person> data, Person person) {
        // Popolo la tabella
        this.table.setItems(data);

        // Creo le textbox e copio i dati inseriti
        this.nomeText.setText(person.getNome());
        this.cognomeText.setText(person.getCognome());
        this.recapitoText.setText(person.getRecapito());
        this.indirizzoText.setText(person.getIndirizzo());
        this.emailText.setText(person.getEmail());
        this.cittàText.setText(person.getTextCittà());

        // Abilito l'autocompletamento per la città
        try {
            TextFields.bindAutoCompletion(cittàText, CittàTable.getCittà());
        } catch (ClassNotFoundException | SQLException e) {

            ApplicationManager.viewExceptionAlert(e, "Errore nell'aggiungere l'autocompletamento a città!");
        }

    }

}
