package app.controller;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTimePicker;

import app.ApplicationManager;
import app.database.OperationTable;
import app.model.Operation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

public class NewOperationController implements Initializable {

    @FXML
    private JFXDatePicker dataInizio;

    @FXML
    private JFXTimePicker oraInizio;

    @FXML
    private JFXDatePicker dataFine;

    @FXML
    private JFXTimePicker oraFine;

    @FXML
    private JFXTextArea descrizione;

    private int idfixing;

    @FXML
    void SetDateAndTime(ActionEvent event) {
        this.dataFine.setValue(LocalDate.now());
        this.oraFine.setValue(LocalTime.now());
    }

    @FXML
    void createNew(ActionEvent event) {
        try {
            Operation o = new Operation(this.dataInizio.getValue(), this.oraInizio.getValue(), this.dataFine.getValue(),
                    this.oraFine.getValue(), this.descrizione.getText(), ApplicationManager.getUsername());

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Conferma");
            alert.setHeaderText("Sicuro di voler completare?");
            alert.setContentText(
                    "Se confermi verrà chiusa la finestra e non potranno essere più eseguite modifiche nè cancellazioni all'intervento!");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                try {
                    OperationTable.addNewOperation(this.idfixing, o);
                    this.closeWindow();
                } catch (ClassNotFoundException | SQLException e) {
                    ApplicationManager.viewExceptionAlert(e, "Impossibile aggiungere l'intervento!");
                }
            }

        } catch (IllegalArgumentException e) {
            ApplicationManager.viewSimpleAlert("Inserisci tutti i parametri necessari!",
                    "Tutti i campi sono obbligatori.", AlertType.ERROR);
        }

    }

    private void closeWindow() {
        Stage stage = (Stage) this.dataFine.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.dataInizio.setValue(LocalDate.now());
        this.oraInizio.setValue(LocalTime.now());
    }

    public void setItem(int idFixing) {
        this.idfixing = idFixing;
    }

}
