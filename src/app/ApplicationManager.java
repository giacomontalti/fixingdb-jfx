package app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import app.controller.HomeController;
import app.database.DBConnection;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ApplicationManager {

    private static Main primaryApplication;
    private static HomeController homeController;

    private ApplicationManager() {
    }

    public static void setRefToPrimaryStage(Main primaryApp) {
        primaryApplication = primaryApp;
    }

    public static void changeScene(String path) throws IllegalArgumentException, IOException {
        if (path == null || path.isEmpty()) {
            // if the string is not valid
            throw new IllegalArgumentException();
        } else {
            // if it's the first time i've loaded this scene
            Scene sceneToLoad = loadSceneFromString(path);
            primaryApplication.setScene(sceneToLoad);
        }
        primaryApplication.centerOnScreen();
    }

    private static Scene loadSceneFromString(String pathToFXML) throws IOException, FileNotFoundException {

        // Create new loader to fxml file passed
        FXMLLoader loader = ApplicationManager.createLoader(pathToFXML);
        Scene sceneToReturn = null;

        // if there isn't any FXML file with this path
        if (loader.getLocation() == null) {
            throw new FileNotFoundException();

        } else {
            // Made a pane with fxml's content loaded
            Pane paneToLoad = loader.load();
            sceneToReturn = new Scene(paneToLoad);
        }
        return sceneToReturn;
    }

    public static void setWindowTitle(String newTitleOfWindow) {
        primaryApplication.setTitle(newTitleOfWindow);
    }

    public static void viewSimpleAlert(String header, String body, AlertType type) {
        Alert dialog = new Alert(type);
        dialog.setContentText(body);
        if (header != null) {
            dialog.setHeaderText(header);
        }
        dialog.show();
    }

    public static void viewExceptionAlert(Exception ex, String header) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Notifica Errore");
        alert.setHeaderText(header);
        alert.setContentText("Copia la stacktrace e inviala per mail allo sviluppatore:");

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public static Stage makeStage(Modality modality, StageStyle stageStyle, String title) {
        Stage s = new Stage(stageStyle);
        s.initModality(modality);
        s.setTitle(title);
        return s;
    }

    public static FXMLLoader createLoader(String nameOfFile) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(ApplicationManager.class.getResource("/app/view/" + nameOfFile + ".fxml"));
        return loader;
    }

    public static void setHomeController(HomeController controller) {
        // TODO Io farei che dopo averne aggiunto uno non si possa più cambiare
        homeController = controller;
    }

    public static void updateView() {
        homeController.updateDashboard();

    }

    public static String getUsername() {
        return DBConnection.getUser();
    }
}
