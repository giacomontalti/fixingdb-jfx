package app.database;

import java.sql.SQLException;
import java.util.List;

public class TipologiaTable {

    private static final String tableName = "tipologia";
    private static final String tableColumn = "Testo";

    static public List<String> getTipologie() throws SQLException, ClassNotFoundException {
        return DBUtils.getSingleListQuery(TipologiaTable.tableName, TipologiaTable.tableColumn);
    }

    static public int getId(String tipologia) throws ClassNotFoundException, SQLException {
        return DBUtils.getSingleIDFromList(tipologia, TipologiaTable.tableName, TipologiaTable.tableColumn);
    }
}
