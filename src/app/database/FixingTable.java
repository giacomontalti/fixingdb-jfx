package app.database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import app.AppUtils;
import app.model.Fixing;
import app.model.StateFixing;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class FixingTable {

    private static String tableName = "Riparazione";

    private static final String selectFixingString = "SELECT R.ID AS idRiparazione, C.ID AS idCliente, "
            + "D.Seriale AS serialeDispositivo, R.Problema_Dichiarato As problemi, "
            + "R.Data_Riconsegna_Richiesta AS riconsegna, R.è_Emergenza AS isEmergenza, "
            + "D.Modello AS modello, T.Testo AS tipologia, M.Nome as marca, C.Nome as nome, "
            + "C.Cognome AS cognome, C.Recapito AS recapito, S.Descrizione as descrizioneStatus, "
            + "R.Data_Entrata as dataEntrata "
            + "FROM riparazione R, stato S, marca M, dispositivo D, cliente C, tipologia T, citta Cit "
            + "WHERE R.SerialeDispositivo=D.Seriale " + "AND R.IDCliente=C.ID " + "AND R.IDStato=S.ID "
            + "AND D.IDTipologia=T.ID " + "AND D.IDMarca=M.ID " + "AND C.IDCitta=Cit.ID";

    private static final String emergencyString = "R.è_Emergenza=1 AND S.Descrizione!='Ritirato'";

    public static void addFixing(Fixing fixing) throws SQLException, ClassNotFoundException {
        Connection connection = DBConnection.getConnection();

        String insertQuery = "INSERT INTO " + FixingTable.tableName
                + "(SerialeDispositivo, IDCliente, Problema_Dichiarato, Visto_Acceso, "
                + "Ha_Alimentatore, Seriale_Alimentatore, Ha_Batteria, Seriale_Batteria, "
                + "Accessori_Note, Data_Riconsegna_Richiesta, è_Emergenza) " + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement insertStatement = connection.prepareStatement(insertQuery);

        // Gestisco la data in caso sia null o meno
        Date riconsegna = null;
        if (fixing.getRiconsegna() != null) {
            riconsegna = AppUtils.fromLocalDateToSqlDate(fixing.getRiconsegna());
        }

        insertStatement.setString(1, fixing.getSerialeDispositivo()); // TODO
                                                                      // Controllare
                                                                      // ogni
                                                                      // Table.java
                                                                      // per
                                                                      // vedere
                                                                      // se
                                                                      // chiudo
                                                                      // o meno
                                                                      // le
                                                                      // connessioni
        insertStatement.setInt(2, fixing.getIdCliente()); // TODO E farlo anche
                                                          // qui
        insertStatement.setString(3, fixing.getProblemi());
        insertStatement.setBoolean(4, fixing.isVistoAcceso());

        insertStatement.setBoolean(5, fixing.isHasAlimentatore());
        insertStatement.setString(6, fixing.getAlimentatore());
        insertStatement.setBoolean(7, fixing.isHasBatteria());
        insertStatement.setString(8, fixing.getBatteria());

        insertStatement.setString(9, fixing.getNoteAccessori());
        insertStatement.setDate(10, riconsegna);
        insertStatement.setBoolean(11, fixing.isEmergenza());

        insertStatement.executeUpdate();
        connection.close();
    }

    public static int countNumOfFixing(String keyword) throws ClassNotFoundException, SQLException {
        Connection connection = DBConnection.getConnection();

        String selectQuery = "SELECT COUNT(*) " + "FROM riparazione R, stato S " + "WHERE R.IDStato=S.ID "
                + "AND S.Descrizione=? " + "GROUP BY R.IDStato";

        PreparedStatement selectStatement = connection.prepareStatement(selectQuery);

        selectStatement.setString(1, keyword);

        ResultSet selectResult = selectStatement.executeQuery();
        // TODO provare a vedere se funziona anche con la connessione chiusa
        if (selectResult.next()) {
            return selectResult.getInt(1);
        } else {
            return 0;
        }
    }

    public static int countNumOfEmergency() throws SQLException, ClassNotFoundException {
        Connection connection = DBConnection.getConnection();

        String selectQuery = "SELECT COUNT(*) FROM riparazione R, stato S WHERE " + emergencyString
                + " AND S.ID=R.IDStato";

        PreparedStatement selectStatement = connection.prepareStatement(selectQuery);

        ResultSet selectResult = selectStatement.executeQuery();
        // TODO provare a vedere se funziona anche con la connessione chiusa
        if (selectResult.next()) {
            return selectResult.getInt(1);
        } else {
            return 0;
        }
    }

    public static ObservableList<FixingExtended> getFixingByState(StateFixing state)
            throws ClassNotFoundException, SQLException {
        ObservableList<FixingExtended> list = FXCollections.observableArrayList();
        List<String> listOfArguments = state.getElement();

        for (String string : listOfArguments) {
            String selectQuery = selectFixingString + " AND S.Descrizione ='" + string + "'"; // FIXME
                                                                                              // Se
                                                                                              // stringa
                                                                                              // ha
                                                                                              // come
                                                                                              // carattere
                                                                                              // '
                                                                                              // fa
                                                                                              // casino
                                                                                              // secondo
                                                                                              // me
            list.addAll(getFixingByQuery(selectQuery));
        }
        return list;

    }

    public static ObservableList<FixingExtended> getEmergency() throws ClassNotFoundException, SQLException {
        String selectQuery = selectFixingString + " AND " + emergencyString;
        return getFixingByQuery(selectQuery);
    }

    private static ObservableList<FixingExtended> getFixingByQuery(String query)
            throws ClassNotFoundException, SQLException {
        ObservableList<FixingExtended> list = FXCollections.observableArrayList();
        Connection connection = DBConnection.getConnection();

        PreparedStatement selectStatement = connection.prepareStatement(query);

        ResultSet selectResult = selectStatement.executeQuery();

        // TODO provare a vedere se funziona anche con la connessione
        // chiusa
        while (selectResult.next()) {
            LocalDate riconsegna = (selectResult.getDate("riconsegna") != null
                    ? selectResult.getDate("riconsegna").toLocalDate() : null);

            list.add(new FixingExtended(selectResult.getInt("idRiparazione"),
                    selectResult.getString("serialeDispositivo"), selectResult.getInt("idCliente"),
                    selectResult.getString("problemi"), riconsegna, selectResult.getBoolean("isEmergenza"),
                    selectResult.getString("modello"), selectResult.getString("tipologia"),
                    selectResult.getString("marca"), selectResult.getString("nome"), selectResult.getString("cognome"),
                    selectResult.getString("recapito"), selectResult.getString("descrizioneStatus"),
                    selectResult.getDate("dataEntrata").toLocalDate()));
        } // TODO Visualizzare data per bene (E guardare anche in PersonTable
          // come fare

        // TODO Ridimensionare colonne della tabella
        return list;
    }

    public static FixingExtended getFixingById(int idFixing) throws ClassNotFoundException, SQLException {
        Connection connection = DBConnection.getConnection();

        String query = "SELECT * FROM riparazione R WHERE R.ID=?";

        PreparedStatement selectStatement = connection.prepareStatement(query);

        selectStatement.setInt(1, idFixing);
        ResultSet selectResult = selectStatement.executeQuery();

        // TODO provare a vedere se funziona anche con la connessione
        // chiusa
        if (selectResult.next()) {
            LocalDate riconsegna = (selectResult.getDate("Data_Riconsegna_Richiesta") != null
                    ? selectResult.getDate("Data_Riconsegna_Richiesta").toLocalDate() : null); // TODO
                                                                                               // Questo
                                                                                               // forse
                                                                                               // lo
                                                                                               // metterei
                                                                                               // in
                                                                                               // apputils

            int idStato = selectResult.getInt("IDStato");

            FixingExtended f = new FixingExtended(selectResult.getInt("ID"),
                    selectResult.getString("problema_Dichiarato"), selectResult.getBoolean("Visto_Acceso"),
                    selectResult.getBoolean("Ha_Alimentatore"), selectResult.getString("Seriale_Alimentatore"),
                    selectResult.getBoolean("Ha_Batteria"), selectResult.getString("Seriale_Batteria"),
                    selectResult.getString("Accessori_Note"), riconsegna, selectResult.getBoolean("è_Emergenza"),
                    idStato, StateTable.getStateById(idStato));

            // TODO Cliente e dispositivo serve?
            return f;

        } else {
            return null;
        }
        // TODO Visualizzare data per bene (E guardare anche in PersonTable
        // come fare
    }

    public static void updateFixing(int id, String stato, Fixing newFixing)
            throws ClassNotFoundException, SQLException {
        Connection connection = DBConnection.getConnection();

        String query = "UPDATE riparazione SET Problema_Dichiarato=?, Visto_Acceso=?, Ha_Alimentatore=?,"
                + " Seriale_Alimentatore=?, Ha_Batteria=?, Seriale_Batteria=?, Accessori_Note=?,"
                + " Data_Riconsegna_Richiesta=?, è_Emergenza=?, IDStato=? WHERE ID=?";

        PreparedStatement selectStatement = connection.prepareStatement(query);

        selectStatement.setString(1, newFixing.getProblemi());
        selectStatement.setBoolean(2, newFixing.isVistoAcceso());
        selectStatement.setBoolean(3, newFixing.isHasAlimentatore());
        selectStatement.setString(4, newFixing.getAlimentatore());
        selectStatement.setBoolean(5, newFixing.isHasBatteria());
        selectStatement.setString(6, newFixing.getBatteria());
        selectStatement.setString(7, newFixing.getNoteAccessori());
        selectStatement.setDate(8, AppUtils.fromLocalDateToSqlDate(newFixing.getRiconsegna()));
        selectStatement.setBoolean(9, newFixing.isEmergenza());
        selectStatement.setInt(10, StateTable.getStateByString(stato));
        selectStatement.setInt(11, id);

        selectStatement.executeUpdate();
    }

    static public class FixingExtended extends Fixing {

        private int idRiparazione;
        private String textEmergenza;
        private LocalDate dataEntrata;
        private String descrizioneStatus;
        private String modello;
        private String tipologia;
        private String marca;
        private String nome;
        private String cognome;
        private String recapito;
        private int idStato;
        private String textStato;

        // Metodo per oggetti a partire dall'id per EditFixingTable
        public FixingExtended(int idRiparazione, String problema, boolean vistoAcceso, boolean haAlimentatore,
                String serialeAlimentatore, boolean haBatteria, String serialeBatteria, String note,
                LocalDate riconsegna, boolean emergenza, int idStato, String textStato) {
            super(problema, vistoAcceso, haAlimentatore, serialeAlimentatore, haBatteria, serialeBatteria, note,
                    riconsegna, emergenza);
            this.idRiparazione = idRiparazione;
            this.idStato = idStato;
            this.textStato = textStato;
        }

        // Medoto per oggetti visualizzabili in FixingTable
        public FixingExtended(int idRiparazione, String serialeDispositivo, int idCliente, String problemi,
                LocalDate riconsegna, boolean isEmergenza, String modello, String tipologia, String marca, String nome,
                String cognome, String recapito, String descrizioneStatus, LocalDate dataEntrata) {
            super(serialeDispositivo, idCliente, problemi, riconsegna, isEmergenza);
            this.idRiparazione = idRiparazione;
            this.modello = modello;
            this.tipologia = tipologia;
            this.marca = marca;
            this.nome = nome;
            this.cognome = cognome;
            this.recapito = recapito;
            this.descrizioneStatus = descrizioneStatus;
            this.dataEntrata = dataEntrata;
            this.textEmergenza = (isEmergenza ? "Si" : "No");
        }

        public int getIdRiparazione() {
            return idRiparazione;
        }

        public void setIdRiparazione(int idRiparazione) {
            this.idRiparazione = idRiparazione;
        }

        public String getTextEmergenza() {
            return textEmergenza;
        }

        public void setTextEmergenza(String textEmergenza) {
            this.textEmergenza = textEmergenza;
        }

        public LocalDate getDataEntrata() {
            return dataEntrata;
        }

        public void setDataEntrata(LocalDate dataEntrata) {
            this.dataEntrata = dataEntrata;
        }

        public String getDescrizioneStatus() {
            return descrizioneStatus;
        }

        public void setDescrizioneStatus(String descrizioneStatus) {
            this.descrizioneStatus = descrizioneStatus;
        }

        public String getModello() {
            return modello;
        }

        public void setModello(String modello) {
            this.modello = modello;
        }

        public String getTipologia() {
            return tipologia;
        }

        public void setTipologia(String tipologia) {
            this.tipologia = tipologia;
        }

        public String getMarca() {
            return marca;
        }

        public void setMarca(String marca) {
            this.marca = marca;
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public String getCognome() {
            return cognome;
        }

        public void setCognome(String cognome) {
            this.cognome = cognome;
        }

        public String getRecapito() {
            return recapito;
        }

        public void setRecapito(String recapito) {
            this.recapito = recapito;
        }

        public int getIdStato() {
            return idStato;
        }

        public void setIdStato(int idStato) {
            this.idStato = idStato;
        }

        public String getTextStato() {
            return textStato;
        }

        public void setTextStato(String textStato) {
            this.textStato = textStato;
        }
    }
}
