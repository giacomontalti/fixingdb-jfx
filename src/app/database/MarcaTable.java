package app.database;

import java.sql.SQLException;
import java.util.List;

public class MarcaTable {
    
    private static String tableName = "Marca";
    private static String tableColumn = "Nome";

    static public List<String> getMarche() throws ClassNotFoundException, SQLException{
        return DBUtils.getSingleListQuery(MarcaTable.tableName, MarcaTable.tableColumn);
    }

    static public int getId(String marca) throws ClassNotFoundException, SQLException {
        return DBUtils.getSingleIDFromList(marca, MarcaTable.tableName, MarcaTable.tableColumn);
    }
}
