package app.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private static String dbName = "fixing_db";
    private static String user = "";
    private static String password = "";
    private static String server = "";
    private static String driver = "org.mariadb.jdbc.Driver";
    private static Connection connection;

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName(driver);
        connection = DriverManager.getConnection("jdbc:mariadb://" + server + ":3306/" + dbName, user, password);
        return connection;
    }

    public static void setLogin(String srvr, String usr, String pswd) {
        user = usr;
        password = pswd;
        server = srvr;
    }

    public static String getUser() {
        return user; // FIXME Ho impostato varchar(20), da rivedere
    }

    // FIXME gli indici bigint vanno salvati su long?
}
