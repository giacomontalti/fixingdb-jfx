package app.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StateTable {

    public static String getStateById(int id) throws ClassNotFoundException, SQLException {
        Connection connection = DBConnection.getConnection();

        String query = "SELECT * FROM stato S WHERE S.ID=?";
        PreparedStatement selectStatement = connection.prepareStatement(query);

        selectStatement.setInt(1, id);

        ResultSet selectResult = selectStatement.executeQuery();

        if (selectResult.next()) {
            return selectResult.getString("Descrizione");
        } else {
            return null;
        }
    }

    public static int getStateByString(String stato) throws ClassNotFoundException, SQLException {
        Connection connection = DBConnection.getConnection();

        String query = "SELECT * FROM stato S WHERE S.Descrizione=?";
        PreparedStatement selectStatement = connection.prepareStatement(query);

        selectStatement.setString(1, stato);

        ResultSet selectResult = selectStatement.executeQuery();

        if (selectResult.next()) {
            return selectResult.getInt("ID");
        } else {
            return 0;
        }    }
}
