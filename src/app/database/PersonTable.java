package app.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import app.ApplicationManager;
import app.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PersonTable {

    static private String tableName = "Cliente";

    public static ObservableList<Person> findPerson(String name, String surname, String telephone)
            throws ClassNotFoundException, SQLException {
        ObservableList<Person> list = FXCollections.observableArrayList();

        String query = "SELECT Cl.ID As IDCliente, Cl.Nome, Cl.Cognome, Cl.Indirizzo, Cl.Recapito, Cl.Email, Cl.IDCitta, Ci.Nome As TextCitta "
                + "FROM  " + PersonTable.tableName + " Cl, citta Ci "
                + "WHERE (Cl.Nome=? AND Cl.Cognome=? OR Cl.Recapito=?) AND Cl.IDCitta=Ci.ID";
        Connection connection = DBConnection.getConnection();
        PreparedStatement statement = connection.prepareStatement(query);

        statement.setString(1, name);
        statement.setString(2, surname);
        statement.setString(3, telephone);

        ResultSet result = statement.executeQuery();

        while (result.next()) {
            list.add(new Person(result.getInt("IDCliente"), result.getString("Nome"), result.getString("Cognome"),
                    result.getString("Recapito"), result.getString("TextCitta"), result.getInt("IDCitta"),
                    result.getString("Indirizzo"), result.getString("Email")));
        }

        connection.close();
        return list;
    }

    // FIXME Qui dovrei scomporre i metodi in due cose separate magari fatte dal
    // model?
    /**
     * Metodo statico che aggiunge ID e IDCittà alla persona e lo aggiunge al ,
     * * database
     * 
     * @param person
     *            la persona da inserire nel database
     * @return la persona inserita nel database con ID e IDCittà settati
     * @throws SQLException
     *             se ci sono problemi con le query
     * @throws ClassNotFoundException
     */
    public static void addPerson(Person person) throws SQLException, ClassNotFoundException {
        // Aggiorno la persona
        if (person.getIdCttà() == 0) {
            try {
                person.setIdCttà(CittàTable.getId(person.getTextCittà()));
            } catch (ClassNotFoundException | SQLException e) {
                ApplicationManager.viewExceptionAlert(e, "Impossibile aggiungere la città alla persona inserita!");
            }
        }

        // Inserisco la persona
        String query2 = "INSERT INTO " + tableName
                + " (Nome, Cognome, Indirizzo, Recapito, Email, IDCitta) VALUES(?,?,?,?,?,?)";
        PreparedStatement statement2 = DBConnection.getConnection().prepareStatement(query2,
                Statement.RETURN_GENERATED_KEYS);

        statement2.setString(1, person.getNome());
        statement2.setString(2, person.getCognome());
        statement2.setString(3, person.getIndirizzo());
        statement2.setString(4, person.getRecapito());
        statement2.setString(5, person.getEmail());
        statement2.setInt(6, person.getIdCttà());

        statement2.executeUpdate();
        ResultSet rs = statement2.getGeneratedKeys();
        if (rs.next()) {
            person.setId(rs.getInt(1));
        }
        statement2.close();
    }
}
