package app.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import app.model.Operation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class OperationTable {

    private static final String tablename = "Intervento";

    static public void addNewOperation(int idFixing, Operation operation) throws ClassNotFoundException, SQLException {
        Connection connection = DBConnection.getConnection();

        String insertQuery = "INSERT INTO " + tablename + "(IDRiparazione, Data_Inizio, Data_Fine, Descrizione, Utente)"
                + " VALUES (?, ?, ?, ?, ?);";

        PreparedStatement statement = connection.prepareStatement(insertQuery);

        statement.setInt(1, idFixing);
        statement.setTimestamp(2, Timestamp.valueOf(operation.getInizio()));
        statement.setTimestamp(3, Timestamp.valueOf(operation.getFine()));
        statement.setString(4, operation.getDescrizione());
        statement.setString(5, operation.getUtente());

        statement.executeUpdate();

        connection.close();
    }

    public static ObservableList<Operation> getOperationsById(int idRiparazione)
            throws ClassNotFoundException, SQLException {
        Connection connection = DBConnection.getConnection();

        ObservableList<Operation> list = FXCollections.observableArrayList();

        String selectQuery = "SELECT * FROM " + tablename + " R WHERE R.IDRiparazione=?";

        PreparedStatement statement = connection.prepareStatement(selectQuery);

        statement.setInt(1, idRiparazione);

        ResultSet result = statement.executeQuery();

        while (result.next()) {
            LocalDateTime inizio = result.getTimestamp("Data_Inizio").toLocalDateTime();
            LocalDateTime fine = result.getTimestamp("Data_Fine").toLocalDateTime();

            list.add(new Operation(inizio.toLocalDate(), inizio.toLocalTime(), fine.toLocalDate(), fine.toLocalTime(),
                    result.getString("Descrizione"), result.getString("Utente")));
        }

        return list;
    }
}
