package app.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import app.model.Device;

public class DispositivoTable {

    private static String tableName = "Dispositivo";

    /**
     * Controlla l'esistenza o meno del seriale
     * 
     * @param serial
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static boolean existSerial(String serial) throws ClassNotFoundException, SQLException {
        return DBUtils.existItem(serial, tableName, "Seriale");
    }

    public static Device loadFromDataBase(String serial) throws SQLException, ClassNotFoundException {
        String selectQuery = "SELECT D.Modello, D.Seriale, M.ID AS IntMarca, T.ID AS IntTipologia, M.Nome AS TextMarca, T.Testo AS TextTipologia "
                + "FROM " + tableName + " D, tipologia T, marca M " + "WHERE D.IDTipologia=T.ID "
                + "AND D.IDMarca=M.ID " + "AND D.Seriale = ?";

        Connection connection = DBConnection.getConnection();
        PreparedStatement selectStatement = connection.prepareStatement(selectQuery);

        selectStatement.setString(1, serial);
        ResultSet selectResult = selectStatement.executeQuery();

        // Se c'è corrispondenza crea il dispositivo e lo ritorna
        if (selectResult.next()) {
            return new Device(selectResult.getString("Seriale"), selectResult.getString("Modello"),
                    selectResult.getInt("intTipologia"), selectResult.getString("textTipologia"),
                    selectResult.getInt("intMarca"), selectResult.getString("textMarca"));
        } else {
            return null;
        }
    }

    public static void addDevice(Device device) throws ClassNotFoundException, SQLException {
        Connection connection = DBConnection.getConnection();

        String insertQuery = "INSERT INTO " + tableName + " (Seriale, Modello, IDTipologia, IDMarca) VALUES(?,?,?,?)";

        PreparedStatement insertStatement = connection.prepareStatement(insertQuery);
        insertStatement.setString(1, device.getSerial());
        insertStatement.setString(2, device.getModel());
        insertStatement.setInt(3, device.getIdTypology());
        insertStatement.setInt(4, device.getIntBrand());
        insertStatement.executeUpdate();        
        connection.close();
    }

}
