package app.database;

import java.sql.SQLException;
import java.util.List;

public class CittàTable {

    private static String tableName = "Citta";
    private static String tableColumn ="Nome";
    
    public static List<String> getCittà() throws ClassNotFoundException, SQLException{
        return DBUtils.getSingleListQuery(CittàTable.tableName, CittàTable.tableColumn);
    }
    
    public static int getId(String città) throws ClassNotFoundException, SQLException{
        return DBUtils.getSingleIDFromList(città, CittàTable.tableName, CittàTable.tableColumn);
    }
}