package app.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBUtils {

    static List<String> getSingleListQuery(String tableName, String tableColumn)
            throws SQLException, ClassNotFoundException {
        List<String> list = new ArrayList<>();

        Connection connection = DBConnection.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM " + tableName);

        while (result.next()) {
            list.add(result.getString(tableColumn));
        }

        statement.close();
        connection.close();
        return list;
    }

    /**
     * Metodo statico che ritorna l'id associato a una tupla in una tabella. Nel
     * caso in cui non esista la tupla, ne crea una nuova e ne ritorna l'id.
     * 
     * @param value
     *            il testo della tupla da ricercare
     * @param tableName
     *            il nome della tabella
     * @param tableColumn
     *            il nome della colonna dove cercare
     * @return l'id della tupla inserita
     * @throws ClassNotFoundException
     *             se non riesco a caricare il driver JDBC
     * @throws SQLException
     *             se genero un qualche errore sql
     */
    static int getSingleIDFromList(String value, String tableName, String tableColumn)
            throws ClassNotFoundException, SQLException {
        // TODO Fare in modo da lanciare eccezione quando value=""
        int id = -1;
        String selectQuery = "SELECT * FROM " + tableName + " WHERE " + tableColumn + "=?";
        Connection connection = DBConnection.getConnection();
        PreparedStatement selectStatement = connection.prepareStatement(selectQuery);

        selectStatement.setString(1, value);
        ResultSet selectResult = selectStatement.executeQuery();

        // Se c'è corrispondenza
        if (selectResult.next()) {
            id = selectResult.getInt(1);
        }
        // Se non ho trovato lo aggiungo
        else {
            id = DBUtils.insertNewAndReturnID(value, tableName, tableColumn);
        }
        connection.close();
        return id;
    }

    static int insertNewAndReturnID(String value, String tableName, String tableColumn)
            throws ClassNotFoundException, SQLException {
        int id = 0;
        Connection connection = DBConnection.getConnection();

        String insertQuery = "INSERT INTO " + tableName + " (" + tableColumn + ") VALUES(?)";

        PreparedStatement insertStatement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
        insertStatement.setString(1, value);
        insertStatement.executeUpdate();
        ResultSet insertResult = insertStatement.getGeneratedKeys();
        if (insertResult.next()) {
            id = insertResult.getInt(1);
        }
        connection.close();
        return id;
    }

    // Molto codice in comune con getSingleIDFromList
    static boolean existItem(String value, String tableName, String tableColumn)
            throws ClassNotFoundException, SQLException {
        String selectQuery = "SELECT * FROM " + tableName + " WHERE " + tableColumn + "=?";
        Connection connection = DBConnection.getConnection();
        PreparedStatement selectStatement = connection.prepareStatement(selectQuery);

        selectStatement.setString(1, value);
        ResultSet selectResult = selectStatement.executeQuery();

        // Se c'è corrispondenza
        if (selectResult.next()) {
            return true;
        } else {
            return false;
        }
    }
}
