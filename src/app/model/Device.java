package app.model;

public class Device {

    private String serial;
    private String model;
    private int idTypology;
    private String textTypology;
    private int intBrand;
    private String textBrand;

    // Per quelli inseriti da tastiera
    public Device(String seriale, String modello, String tipologia, String marca) {
        this.serial = seriale;
        this.model = modello;
        this.textTypology = tipologia;
        this.textBrand = marca;
    }

    // Per quelli caricati da database
    public Device(String serial, String model, int idTypology, String textTypology, int intBrand, String textBrand) {
        super();
        this.serial = serial;
        this.model = model;
        this.idTypology = idTypology;
        this.textTypology = textTypology;
        this.intBrand = intBrand;
        this.textBrand = textBrand;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getIdTypology() {
        return idTypology;
    }

    public void setIdTypology(int idTypology) {
        this.idTypology = idTypology;
    }

    public String getTextTypology() {
        return textTypology;
    }

    public void setTextTypology(String textTypology) {
        this.textTypology = textTypology;
    }

    public int getIntBrand() {
        return intBrand;
    }

    public void setIntBrand(int intBrand) {
        this.intBrand = intBrand;
    }

    public String getTextBrand() {
        return textBrand;
    }

    public void setTextBrand(String textBrand) {
        this.textBrand = textBrand;
    }

    @Override
    public String toString() {
        return "Device [serial=" + serial + ", model=" + model + ", idTypology=" + idTypology + ", textTypology="
                + textTypology + ", intBrand=" + intBrand + ", textBrand=" + textBrand + "]";
    }

}
