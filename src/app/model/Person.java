package app.model;

public class Person {

    //TODO Ristrutturare e togliere tutto ciò che non è della persona come fatto in fixing (è da fare?) e creare una PersonExtended dentro PersonTable
    private int id;
    private String nome;
    private String cognome;
    private String recapito;
    private int idCttà;
    private String textCittà;
    private String indirizzo;
    private String email;

    // Costruttore per database
    public Person(int id, String nome, String cognome, String recapito, String textCittà, int idCittà, String indirizzo,
            String email) {
        this(nome, cognome, recapito, textCittà, indirizzo, email);
        this.id = id;
        this.idCttà = idCittà;
    }

    // Costruttore per HomeController
    public Person(String nome, String cognome, String recapito, String textCittà, String indirizzo, String email) {
        this.nome = nome;
        this.cognome = cognome;
        this.recapito = recapito;
        this.textCittà = textCittà;
        this.indirizzo = indirizzo;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getRecapito() {
        return recapito;
    }

    public void setRecapito(String recapito) {
        this.recapito = recapito;
    }

    public int getIdCttà() {
        return idCttà;
    }

    public void setIdCttà(int idCttà) {
        this.idCttà = idCttà;
    }

    public String getTextCittà() {
        return textCittà;
    }

    public void setTextCittà(String textCittà) {
        this.textCittà = textCittà;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Person [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", recapito=" + recapito + ", idCttà="
                + idCttà + ", textCittà=" + textCittà + ", indirizzo=" + indirizzo + ", email=" + email + "]";
    }

}