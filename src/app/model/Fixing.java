package app.model;

import java.time.LocalDate;

public class Fixing {

    private String serialeDispositivo;
    private int idCliente;
    private String problemi;
    private boolean vistoAcceso;
    private boolean hasAlimentatore;
    private String alimentatore;
    private boolean hasBatteria;
    private String batteria;
    private String noteAccessori;
    private LocalDate riconsegna;
    private boolean isEmergenza;

    // Utile per estendere la classe e costruire il tipo di dato ExtendedFixing
    // Comprende i campi di Fixing visualizzati nella tabella riparazioni
    protected Fixing(String serialeDispositivo, int idCliente, String problemi, LocalDate riconsegna,
            boolean isEmergenza) {
        this.serialeDispositivo = serialeDispositivo;
        this.idCliente = idCliente;
        this.problemi = problemi;
        this.riconsegna = riconsegna;
        this.isEmergenza = isEmergenza;
    }

    public Fixing(String problema, boolean vistoAcceso, boolean haAlimentatore, String serialeAlimentatore,
            boolean haBatteria, String serialeBatteria, String note, LocalDate riconsegna, boolean emergenza) {
        this.problemi = problema;
        this.vistoAcceso = vistoAcceso;
        this.hasAlimentatore = haAlimentatore;
        this.alimentatore = serialeAlimentatore;
        this.hasBatteria = haBatteria;
        this.batteria = serialeBatteria;
        this.noteAccessori = note;
        this.riconsegna = riconsegna;
        this.isEmergenza = emergenza;
    }

    // Utile per
    public Fixing(String serialeDispositivo, int idCliente, String problema, boolean vistoAcceso,
            boolean haAlimentatore, String serialeAlimentatore, boolean haBatteria, String serialeBatteria, String note,
            LocalDate riconsegna, boolean emergenza) {
        this(problema, vistoAcceso, haAlimentatore, serialeAlimentatore, haBatteria, serialeBatteria, note, riconsegna,
                emergenza);
        this.serialeDispositivo = serialeDispositivo;
        this.idCliente = idCliente;
    }

    public String getSerialeDispositivo() {
        return serialeDispositivo;
    }

    public void setSerialeDispositivo(String serialeDispositivo) {
        this.serialeDispositivo = serialeDispositivo;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getProblemi() {
        return problemi;
    }

    public void setProblemi(String problemi) {
        this.problemi = problemi;
    }

    public boolean isVistoAcceso() {
        return vistoAcceso;
    }

    public void setVistoAcceso(boolean vistoAcceso) {
        this.vistoAcceso = vistoAcceso;
    }

    public boolean isHasAlimentatore() {
        return hasAlimentatore;
    }

    public void setHasAlimentatore(boolean hasAlimentatore) {
        this.hasAlimentatore = hasAlimentatore;
    }

    public String getAlimentatore() {
        return alimentatore;
    }

    public void setAlimentatore(String alimentatore) {
        this.alimentatore = alimentatore;
    }

    public boolean isHasBatteria() {
        return hasBatteria;
    }

    public void setHasBatteria(boolean hasBatteria) {
        this.hasBatteria = hasBatteria;
    }

    public String getBatteria() {
        return batteria;
    }

    public void setBatteria(String batteria) {
        this.batteria = batteria;
    }

    public String getNoteAccessori() {
        return noteAccessori;
    }

    public void setNoteAccessori(String noteAccessori) {
        this.noteAccessori = noteAccessori;
    }

    public LocalDate getRiconsegna() {
        return riconsegna;
    }

    public void setRiconsegna(LocalDate riconsegna) {
        this.riconsegna = riconsegna;
    }

    public boolean isEmergenza() {
        return isEmergenza;
    }

    public void setEmergenza(boolean isEmergenza) {
        this.isEmergenza = isEmergenza;
    }

    @Override
    public String toString() {
        return "Fixing [serialeDispositivo=" + serialeDispositivo + ", idCliente=" + idCliente + ", problemi="
                + problemi + ", vistoAcceso=" + vistoAcceso + ", hasAlimentatore=" + hasAlimentatore + ", alimentatore="
                + alimentatore + ", hasBatteria=" + hasBatteria + ", batteria=" + batteria + ", noteAccessori="
                + noteAccessori + ", riconsegna=" + riconsegna + ", isEmergenza=" + isEmergenza + "]";
    }

}
