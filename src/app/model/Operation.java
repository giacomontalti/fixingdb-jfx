package app.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Operation {

    int id;
    LocalDateTime inizio;
    LocalDateTime fine;
    String descrizione;
    String utente;

    public Operation(LocalDate dataInizio, LocalTime oraInizio, LocalDate dataFine, LocalTime oraFine,
            String descrizione, String utente) {
        if (dataInizio == null || dataFine == null || oraInizio == null || oraFine == null || utente == null
                || utente.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.inizio = LocalDateTime.of(dataInizio, oraInizio);
        this.fine = LocalDateTime.of(dataFine, oraFine);
        this.descrizione = descrizione;
        this.utente = utente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getInizio() {
        return inizio;
    }

    public void setInizio(LocalDateTime inizio) {
        this.inizio = inizio;
    }

    public LocalDateTime getFine() {
        return fine;
    }

    public void setFine(LocalDateTime fine) {
        this.fine = fine;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

}