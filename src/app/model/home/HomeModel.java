package app.model.home;

import java.sql.SQLException;

import app.database.FixingTable.FixingExtended;
import app.model.Device;
import app.model.Fixing;
import app.model.Person;
import app.model.StateFixing;
import javafx.collections.ObservableList;

public interface HomeModel {

    //FIXME Questo verrà cancellato
    String generateSeriale() throws ClassNotFoundException, SQLException;

    String[] getTipologie() throws ClassNotFoundException, SQLException;

    String[] getMarche() throws ClassNotFoundException, SQLException;

    String[] getCittà() throws ClassNotFoundException, SQLException;

    ObservableList<Person> findPerson(Person person) throws ClassNotFoundException, SQLException;

    void correctDevice(Device device) throws ClassNotFoundException, SQLException;
    
    void addPerson(Person person) throws ClassNotFoundException, SQLException;

    void setPerson(Person person) throws IllegalArgumentException;

    Person getSelectedPerson();

    Device getSelectedDevice();
    
    void addFixing(Fixing fixing) throws ClassNotFoundException, SQLException;

    void reset();

    int getCountOf(StateFixing typeOfStatus) throws ClassNotFoundException, SQLException;
    
    ObservableList<FixingExtended> getListOfFixing(StateFixing state) throws ClassNotFoundException, SQLException;
}
