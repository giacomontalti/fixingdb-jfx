package app.model.home;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import app.database.CittàTable;
import app.database.DispositivoTable;
import app.database.FixingTable;
import app.database.MarcaTable;
import app.database.PersonTable;
import app.database.TipologiaTable;
import app.database.FixingTable.FixingExtended;
import app.model.Device;
import app.model.Fixing;
import app.model.Person;
import app.model.StateFixing;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class HomeModelImpl implements HomeModel {

    final private static int LENGTH_OF_SERIAL = 10;

    private Device device;
    private Person person;

    public HomeModelImpl() {
    }

    @Override
    public String[] getTipologie() throws ClassNotFoundException, SQLException {
        return TipologiaTable.getTipologie().toArray(new String[0]);
    }

    @Override
    public String[] getMarche() throws ClassNotFoundException, SQLException {
        return MarcaTable.getMarche().toArray(new String[0]);
    }

    @Override
    public String[] getCittà() throws ClassNotFoundException, SQLException {
        return CittàTable.getCittà().toArray(new String[0]);
    }

    @Override
    public String generateSeriale() throws ClassNotFoundException, SQLException {
        String candidateSerial;
        do {
            candidateSerial = RandomStringUtils.random(LENGTH_OF_SERIAL, true, true).toUpperCase();
        } while (DispositivoTable.existSerial(candidateSerial));
        return candidateSerial;
    }

    @Override
    public ObservableList<Person> findPerson(Person person) throws ClassNotFoundException, SQLException {
        ObservableList<Person> list = FXCollections.observableArrayList();
        list = PersonTable.findPerson(person.getNome(), person.getCognome(), person.getRecapito());
        return list;
    }

    @Override
    public void correctDevice(Device device) throws ClassNotFoundException, SQLException {
        if (DispositivoTable.existSerial(device.getSerial())) {
            device = DispositivoTable.loadFromDataBase(device.getSerial());
        } else {
            if (device.getIdTypology() == 0) {
                device.setIdTypology(TipologiaTable.getId(device.getTextTypology()));
            }

            if (device.getIntBrand() == 0) {
                device.setIntBrand(MarcaTable.getId(device.getTextBrand()));
            }
            DispositivoTable.addDevice(device);
        }
        this.device = device;

    }

    @Override
    public void addPerson(Person person) throws ClassNotFoundException, SQLException {
        // TODO Controllare campi prima di inserimento
        PersonTable.addPerson(person);
        this.setPerson(person);
    }

    @Override
    public void setPerson(Person person) {
        if (person.getId() == 0) {
            throw new IllegalArgumentException();
        } else {
            this.person = person;
        }
    }

    @Override
    public Person getSelectedPerson() {
        // TODO Controllare tutti i campi necessari e guardare cosa succede in
        // home controller se device - person - fixing hanno cose sbagliate
        return this.person;
    }

    @Override
    public Device getSelectedDevice() {
        // TODO Controllare tutti i campi
        return this.device;
    }

    @Override
    public void addFixing(Fixing fixing) throws ClassNotFoundException, SQLException {
        // TODO controllare se data è dopo l'attuale, se ha id persona e id
        // device ( e se magari sono quelli selezionati)
        FixingTable.addFixing(fixing);
    }

    @Override
    public void reset() {
        this.device = null;
        this.person = null;
    }

    @Override
    public int getCountOf(StateFixing typeOfStatus) throws ClassNotFoundException, SQLException {
        if (typeOfStatus == StateFixing.EMERGENCY) {
            return FixingTable.countNumOfEmergency();
        } else {
            List<String> list = typeOfStatus.getElement();
            int counter = 0;
            for (String string : list) {
                counter = counter + FixingTable.countNumOfFixing(string);
            }
            return counter;
        }
    }

    @Override
    public ObservableList<FixingExtended> getListOfFixing(StateFixing state)
            throws ClassNotFoundException, SQLException {
        if (state == StateFixing.EMERGENCY) {
            return FixingTable.getEmergency();
        } else {
            return FixingTable.getFixingByState(state);
        }
    }
}
