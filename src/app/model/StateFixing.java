package app.model;

import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Arrays;
import java.util.Iterator;

public enum StateFixing {

    TO_START(Arrays.asList("Da iniziare")), WORKING_ON(Arrays.asList("In lavorazione")), TO_CALL(
            Arrays.asList("Completato, da chiamare", "Chiamare per preventivo")), TO_DELIVER(
                    Arrays.asList("Cliente in arrivo")), PICKED_UP(Arrays.asList("Ritirato")), TO_RESUMING(
                            Arrays.asList("Preventivo Accettato", "Preventivo Rifiutato")), EMERGENCY(
                                    Arrays.asList("Emergenze"));

    private List<String> paroleChiave;

    StateFixing(List<String> alias) {
        this.paroleChiave = alias;
    }

    public List<String> getElement() {
        return this.paroleChiave;
    }

    public static ObservableList<String> getAsList() {
        ObservableList<String> list = FXCollections.observableArrayList();
        for (StateFixing state : StateFixing.values()) {
            if (state.getElement() != null) {
                list.addAll(state.getElement());
            }
        }
        return list;
    }

    @Override
    public String toString() {
        Iterator<String> iterator = paroleChiave.iterator();
        String string = "";

        if (iterator.hasNext()) {
            string = string + " " + iterator.next();
            while (iterator.hasNext()) {
                string = string + " | " + iterator.next();
            }
        }

        return string;
    }
}
