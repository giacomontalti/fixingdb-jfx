package app.model.login;

import java.sql.SQLException;

public interface LoginModel {

    void tryConnection(String serverAddress, String usr, String psswrd) throws ClassNotFoundException, SQLException;
}
