package app.model.login;

import java.sql.SQLException;

import app.database.DBConnection;

public class LoginModelImpl implements LoginModel {

    @Override
    public void tryConnection(String server, String usr, String psswrd) throws ClassNotFoundException, SQLException {
        DBConnection.setLogin(server, usr, psswrd);
        DBConnection.getConnection();
    }
}
