package app;

import java.sql.Date;
import java.time.LocalDate;

public class AppUtils {

    public static Date fromLocalDateToSqlDate(LocalDate date) {
        if (date != null) {
            return Date.valueOf(date);
        } else {
            return null;
        }
    }
}
